package nl.utwente.di.fahrenheitConverter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Test the Quoter
 */
public class TestConverter {

    @Test
    public void testConvertion1() {
        Converter converter = new Converter();
        double celsius_temp = converter.convertCToF(1.);
        Assertions.assertEquals(33.8, celsius_temp, 0., "temperature conversion for 1°C");
    }
}
